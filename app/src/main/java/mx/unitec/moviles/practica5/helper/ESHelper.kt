package mx.unitec.moviles.practica5.helper

import android.os.Environment

class ESHelper {
    companion object{
        fun isWritable(): Boolean {
            return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
        }
     fun isReadable(): Boolean{
         return Environment.getExternalStorageState()in
                 setOf(Environment.MEDIA_MOUNTED, Environment.MEDIA_MOUNTED_READ_ONLY)
     }
    }
}